import numpy as np
import copy

class environment:                                       # Umgebungsklasse

    def __init__(self):                                  # Initialisiere Umgebung
        self.environment_Data = -1 * np.ones((6,8))      # Reward fuer der normalen Felder
        self.environment_Data[1,4] = self.environment_Data[2,3] = self.environment_Data[2,7] = self.environment_Data[3,0] = self.environment_Data[4,6] = self.environment_Data[5,2]= -1000   # Reward fuer die Venusfliegenfalle
        self.environment_Data[3,4] = 1000                # Reward fuer das Wurstbrot
        self.fly_x = 0; self.fly_y = 3                   # Start-Position der Fliege im System
    
    def set_fly(self, x=0, y=3):                         # Setz-Funktion fuer die Fliege
        self.fly_x = x
        self.fly_y = y

    def move(self, a):                                 # Bewegt die Fliege in der Umgebung und gibt reward zuruek 
        if a == 0: self.fly_y -= 1                     # Osten
        if a == 1: self.fly_x += 1                     # Sueden
        if a == 2: self.fly_y += 1                     # Westen
        if a == 3: self.fly_x -= 1                     # Norden
        if self.fly_x < 0 or self.fly_x > 5 or self.fly_y < 0 or self.fly_y > 7:   # Prueft ob die Umgebung verlassen wurde
            r = -10                                    # Reward fuer das verlassen der Umgebung
        else:
            r = self.environment_Data[self.fly_x, self.fly_y]   # Reward der Umgebung
        if self.fly_x < 0: self.fly_x = 0              # Bedingungen setzen Fliege zurueck, sollte die Karte verlassen werden
        if self.fly_x > 5: self.fly_x = 5
        if self.fly_y < 0: self.fly_y = 0
        if self.fly_y > 7: self.fly_y = 7
        return (r)                                     # Gibt Reward zurueck
    
    def sensors(self):                        # Gibt Position der Fliege in der Umgebung zurueck
        return (self.fly_x, self.fly_y) 
    
#################################################################################################

class Learning_Fly:                           # Lern-Klasse

    def __init__(self, env, vareps):          # Initialisiert: 
        self.env = env                        # die Umgebung,
        self.hatQ = np.zeros((6,8,4))         # erstellt die Q-Tabelle fuer jedes Aktions-Zustandspaar und
        self.vareps = vareps                  # das epsilon des e-greedy Ansatzes
    
    def _chooseAction(self, x, y):            # Waehlt Aktion aus:
        # e-greedy Ansatz
        fate = np.random.rand(1)
        if fate < self.vareps:
            a = np.random.randint(4)          # Zufaellige Aktion, wenn zufall < epsilon
        else:
            localQ = self.hatQ[x,y,:]
            a = np.argmax(localQ)             # Aktion fuer die der erwartete Reward maximal ist
        return a
    
    def learn(self, gamma, trainCycles):    # Lernt die Q-Funktion basierend auf gemachten Erfahrungen
        for i in range(trainCycles):        # Durchlaeufe der Lernzyklen
            (x,y) = self.env.sensors()                                                 
            a = self._chooseAction(x, y)
            r = self.env.move(a)
            (xNew, yNew) = self.env.sensors()
            localQ = self.hatQ[xNew, yNew,:]              # Waehlt alte erwartete Rewards an neuer Position aus             
            self.hatQ[x,y,a] = r + gamma * np.max(localQ) # Berechne neue erwartete Rewards an alter Position fuer Aktion a                                   
            if r == 1000: self.env.set_fly()              # Bei erreichen des Wurstbrots wird das Spiel neu gestartet
            percent = 100/trainCycles * i                                                   
            if percent % 1 == 0: print (percent,"%")      # Gibt den Status des Lernvorgangs in Prozent aus
    
    def show(self, x=0, y=3, showGrid = False):               # Zeigt Aktion und Reward der Fliege an...
        self.env.set_fly(x,y)
        r = -1
        counter = 0
        while r != 1000 and counter < 200:  # Zeigt die Fliege samt Aktion und Reward an, bis sie es zum Wurstbrot schafft.
            counter += 1
            (x,y) = self.env.sensors()
            a = self._chooseAction(x,y)
            r = self.env.move(a)
            print(a,r)
            with open("results.txt", "a") as result:
            	if counter == 1:
            		result.write("Start Position: X = " + str(x) + " und Y = " + str(y)+"\n")
            	result.write("action: "+ str(a) +"\treward: "+str(r)+"\n")
            	if r == 1000:
            		result.write("End\n\n")
            if showGrid:
            	envShow = copy.deepcopy(self.env.environment_Data)  # ...und schreibt die Umgebung in die Konsole fuer jeden Schritt
            	envShow[self.env.fly_x, self.env.fly_y] = 8
            	print(envShow)

####################################################################################################   

if __name__ == '__main__':                  # Startet die Simulation
    np.random.seed(42)
    my_environment = environment()
    puck = Learning_Fly(my_environment, 0.2)
    puck.learn(0.4, 200000)
    print ('Starte bei x = 0 und y = 3')
    puck.show()
    print ('Starte bei x = 5 und y = 7')
    puck.show(5,7)
    
