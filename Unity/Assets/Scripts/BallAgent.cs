﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BallAgent : Agent
{
    // Properties
    public Transform Target;
	public Transform[] Collectables;
	public Vector3 StartingPosition;

    // Constants
	private float collectableLowerBounds = 0.3f;
	private float collectableHigherBoundsZ = 1.4f; //1.55
	private float collectableHigherBoundsX = 1.0f; //1.15
    private float collectableSpace = 0.03f;

    private float AccelerationFactor = 1.8547f;
    private const int normalizationValue = 10;
    private float maxVelocity = 1.976f;

    // Vars
    private Rigidbody rigidBody;
    private Vector3 oldPosition;
    private float currentVelocity;
    private GameObject goal;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        goal = GameObject.FindGameObjectWithTag("goal");
    }

    public override void AgentReset()
    {
		//// Reset Collectables
        // Temporäre Liste für Positionsvergleiche erstellen
        List<Vector3> tempCollectablePositions = new List<Vector3>();

        for (int i = 0; i < Collectables.Length; i++)
        {
            // Alte Position holen
            Vector3 collectablePosition = Collectables[i].position;
            collectablePosition.y = 0.036f;

            // Überlappungen vermeiden
            bool overlappingX = true;
            bool overlappingZ = true;
            while(overlappingX || overlappingZ)
            {
                // Wenn die Position noch überlappt, eine neue Bestimmen
                if(overlappingX)
                {
                    float posX = Random.Range(collectableLowerBounds, collectableHigherBoundsX) * RandomPositiveNegative();

                    // Wenn die neue Posistion mit keiner alten Position überlappt
                    if (!tempCollectablePositions.Any(t => posX < t.x + collectableSpace && posX > t.x - collectableSpace))
                    {
                        collectablePosition.x = posX;
                        overlappingX = false;
                    }
                }

                // Wenn die Position noch überlappt, eine neue Bestimmen
                if (overlappingZ)
                {
                    float posZ = Random.Range(collectableLowerBounds, collectableHigherBoundsZ) * RandomPositiveNegative();

                    // Wenn die neue Posistion mit keiner alten Position überlappt
                    if (!tempCollectablePositions.Any(t => posZ < t.z + collectableSpace && posZ > t.z - collectableSpace))
                    {
                        collectablePosition.z = posZ;
                        overlappingZ = false;
                    }
                }
                
            }

            // Es gibt keine Überlappungen mehr -> Position speichern
            Collectables[i].position = collectablePosition;
            // Collectable zurücksetzen
            Collectables[i].gameObject.SetActive(true);
            Collectables[i].gameObject.GetComponent<CollectableBehaviour>().Status = 1;
            // Position der Liste für Vergleiche mit anderen Position hinzufügen.
            tempCollectablePositions.Add(collectablePosition);
        }

        // Reset Target
        goal.SetActive(false);

        // Reset Ball
        rigidBody.angularVelocity = Vector3.zero;
		rigidBody.velocity = Vector3.zero;
		transform.position = StartingPosition;
    }

	public override void CollectObservations()
	{
		// Ball Position
		AddVectorObs (transform.position.x / normalizationValue);
		AddVectorObs (transform.position.z / normalizationValue);

		// Ball Velocity
		//currentVelocity = ((transform.position - oldPosition) / Time.deltaTime).magnitude;
		//AddVectorObs(currentVelocity / normalizationValue);

        // Ball Direction
        //AddVectorObs(transform.rotation.y / 360);
		float vx = rigidBody.velocity.x / maxVelocity;
		float vz = rigidBody.velocity.z / maxVelocity;
		AddVectorObs(vx);
		AddVectorObs(vz);
        Debug.ClearDeveloperConsole();
		Debug.ClearDeveloperConsole();
        Debug.LogError(string.Format("Current Speed: {0}, Angle: {1}", currentVelocity, transform.rotation.y));

        // Collectable Distances
		for (int i = 0; i < Collectables.Length; i++)
		{
			if (Collectables [i].gameObject.GetComponent<CollectableBehaviour> ().Status == 1)
			{
				Vector3 distance = (Collectables [i].position - transform.position);
				AddVectorObs (distance.x/normalizationValue);
				AddVectorObs (distance.z/normalizationValue);
			}
			else
			{
				AddVectorObs (1);
				AddVectorObs (1);
			}

            AddVectorObs(Collectables[i].gameObject.GetComponent<CollectableBehaviour> ().Status);
            // Debug.LogError(string.Format("Collectable {0}, Status {1}, Distance {2}, {3}", i, Collectables[i].gameObject.GetComponent<CollectableBehaviour>().Status, distance.x, distance.z));
		}

        // Check if all Collectables are collected
        if(Collectables.All(x => x.gameObject.GetComponent<CollectableBehaviour>().Status == 0))
        {
            if(!goal.activeSelf)
            {
                goal.SetActive(true);
            }

            AddVectorObs(1);
        }
        else
        {
            AddVectorObs(0);
        }

        // Set Oldposition for calculating the velocity
        oldPosition = transform.position;
	}

	public override void AgentAction(float[] vectorAction, string textAction)
	{
		// Rewards
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, transform.localScale.x / 2);

		foreach (var collider in hitColliders)
		{
			if (collider.gameObject.CompareTag ("goal"))
			{
                Debug.Log("Ziel getroffen.");
				AddReward (100);
				Done ();
			}

			// Collectable collected
			else if (collider.gameObject.CompareTag ("Pick Up"))
			{
                Debug.Log("Collectable gesammelt.");
				AddReward (10);
				collider.gameObject.GetComponent<CollectableBehaviour>().Status = 0;
				collider.gameObject.SetActive (false);
			} 

			// Wall got hit
			else if (collider.gameObject.CompareTag ("wall"))
			{
                Debug.Log("Hindernis getroffen.");
				AddReward (1);
			}
		}

        // Calculate Ballmovement
        Vector3 direction = new Vector3()
        {
            x = Mathf.Clamp(vectorAction[0], -1, 1),
            z = Mathf.Clamp(vectorAction[1], -1, 1)
        };
        // angle in range from -180 to 180 degree
        float angle = Vector3.SignedAngle(Vector3.forward, direction, Vector3.up);
        float acceleration = 0.6f * AccelerationFactor;
        Vector3 accelerationVector = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.forward * acceleration;

        // if the maximum speed is not reached -> add force
        if (currentVelocity < maxVelocity)
        {
            rigidBody.AddForce(accelerationVector, ForceMode.Acceleration);
        }
	}

	private int RandomPositiveNegative()
	{
		return (Random.Range(0, 2) * 2 - 1);
	}
}