##### Imports #####
from unityagents import UnityEnvironment
import matplotlib.pyplot as plt
import numpy as np
import sys
#from agent import DQNAgent
##### End Imports #####


##### Params #####
# Name of the environment binary (located in the root directory of the python project).
UNITY_FILENAME = "../Builds/LearningBall/LearningBall"
# Indicates which port to use for communication with the environment. For use in parallel training
UNITY_WORKERID = 0
# Seed
UNITY_SEED = 1
# Agentnumber
AGENT_NUMBER = 0

# TrainMode
TRAIN_MODE = True
# Number of Episodes
MAX_EPISODES = 1000
# Network-Size
INPUTSIZE = 11
ACTIONSIZE = 3
##### End Params #####

# Variables
xOld = 0
zOld = 0
deltaTime = 0.02
velocity = 0
normalizationValue = 10

##### Code #####
#%matplotlib inline

print("Python version:")
print(sys.version)

# check Python version
if (sys.version_info[0] < 3):
    raise Exception("ERROR: ML-Agents (v0.3 onwards) requires Python 3")


# Creating the environment
print("Start UnityEnvironment. This may take a while...")
unityEnvironment = UnityEnvironment(UNITY_FILENAME, UNITY_WORKERID, UNITY_SEED)
print("UnityEnvironment started.")
# print(str(unityEnvironment))

# # Create the agent
# agent = DQNAgent(inputSize = INPUTSIZE, actionSize = ACTIONSIZE)
# print("DQNAgent created.")

# Get the default brain --> I think, that we control multiple brains with indexing
defaultBrainName = unityEnvironment.brain_names[0]
brain = unityEnvironment.brains[defaultBrainName]

print("Start learning...")
for episode in range(MAX_EPISODES):
    # Reset the environment
    brainInfo = unityEnvironment.reset(train_mode=TRAIN_MODE)[defaultBrainName]
    episodeDone = False
    episodeRewards = 0
    episodesSteps = 0

    # Run the episode
    while not episodeDone:
        # Increase the stepcounter
        episodesSteps += 1

        # Get the current state
        state = np.reshape(brainInfo.vector_observations, [1, INPUTSIZE])
        #velocity = (((state[0,0] - xOld)**2 + (state[0,1] - zOld)**2)**0.5/deltaTime)
        #if velocity == 0:
        #	print("Hit Wall by step: ", episodesSteps)
        #print("##### State ##### ")
        #print("# Ball Position X, Z: {}, {}".format(state[0,0], state[0,1]))
        #print("# Ball Velocity: ", velocity)
        #print("# Target Position X, Z: {}, {}".format(state[0,2], state[0,3]))
        #for i in range(4, 19, 3):
        #    print("# Collectable X, Z, Status: {}, {}, {}".format(state[0,i], state[0,i + 1], state[0,i + 2]))
        print("")


        if (episodeRewards > -100):
            #controlInput = input("w a s d:")
            action = [0, 1, 0]
        else:
            action = [0, 0, 0]
            print(episodesSteps)
        """if(controlInput == "w"):
            action[0] = 0
            action[1] = 0.2
        elif(controlInput == "a"):
            action[0] = 0.25
            action[1] = 0.2
        elif(controlInput == "s"):
            action[0] = 0.5
            action[1] = 0.2
        elif(controlInput == "d"):
            action[0] = 1
            action[1] = 0.2"""

        actionDict = {defaultBrainName:action}
        print("Actions: " + str(actionDict))
        xOld = state[0,0]
        zOld = state[0,1]
        # Perform the action in Unity
        brainInfo = unityEnvironment.step(vector_action=actionDict)[defaultBrainName]
        # Get the next state
        nextState = np.reshape(brainInfo.vector_observations, [1,INPUTSIZE])

        # Get the information from the brain
        stepReward = brainInfo.rewards[AGENT_NUMBER]
        episodeRewards += stepReward
        episodeDone = brainInfo.local_done[AGENT_NUMBER]

        # Prepare state for the next step
        state = nextState

        if episodeRewards < -500 :
            episodeDone = True
        

        if episodeDone:
            print("Episode {}/{} finished, Reward: {}, Steps: {}".format(episode, MAX_EPISODES, episodeRewards, episodesSteps))

    


# Close the environment and close the connection
unityEnvironment.close()

