##### Imports #####
from collections import deque
from keras.layers import Input, Dense
from keras.models import Model
from keras.models import model_from_json
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping
from keras.regularizers import l2
import numpy as np
import os
import datetime
import glob
##### End Imports #####

##### Constants #####
#MODEL_PATH = "../../Keras Models/"                  # PC 1
#MODEL_PATH = "../../../../PC_2/Keras Models/"       # PC 2
MODEL_PATH = "../../../../PC_3/Keras Models/"       # PC 3
#MODEL_PATH = "../../../../PC_4/Keras Models/"       # PC 4
#MODEL_PATH = "../../../../PC_5/Keras Models/"       # PC 5
#MEMORIES_PATH = "../../Memories/memories.npy"       # PC_1
#MEMORIES_PATH = "../../../../PC_2/Memories/memories.npy"       # PC_2
MEMORIES_PATH = "../../../../PC_3/Memories/memories.npy"       # PC_3
#MEMORIES_PATH = "../../../../PC_4/Memories/memories.npy"       # PC_4
#MEMORIES_PATH = "../../../../PC_5/Memories/memories.npy"       # PC_5
#REWARDS_PATH = "../../Memories/rewards.npy"
#REWARDS_PATH = "../../../../PC_2/Memories/rewards.npy"       # PC_2
REWARDS_PATH = "../../../../PC_3/Memories/rewards.npy"       # PC_3
#REWARDS_PATH = "../../../../PC_4/Memories/rewards.npy"       # PC_4
#REWARDS_PATH = "../../../../PC_5/Memories/rewards.npy"       # PC_5

POSSIBLE_DIRECTIONS = np.array([-1, 0, 1])
POSSIBLE_FORCES = np.array([0.6])
#####m End Constants #####

##### Agent #####
class DQNAgent:
    def __init__(self, stateSize, actionSize, rewardGoal, rewardCollectable, rewardWall, rewardStep):
        print("##### Initializing the DQNAgent")
        self.folderName = ""
        self.maxMemory = 10000000
        self.saveCounter = -1
        self.stateSize = stateSize
        self.actionSize = actionSize
        self.alpha = 0.4
        self.gamma = 0.9
        self.tau = 1
        self.learningrate = 0.001
        self.saveMemoriesFrequency = 3000
        self.initialLearnTreshold = 100000
        self.batchSize = 50000
        self.updateFrequency = 500
        self.possibleActions = self._initActions()
        self.memoryStates = np.inf*np.ones((self.maxMemory, self.stateSize))
        self.memoryActions = np.inf*np.ones((self.maxMemory, self.actionSize))
        self.memoryRewards = np.zeros((self.maxMemory, 1))
        self.memoryCounter = -1
        self.maxBadMemory = 0.2
        self.maxGoodMemory = 0.2
        self.qModel = self._buildModel()
        self.initPhase = True
        self.rewardGoal = rewardGoal
        self.rewardCollectable = rewardCollectable
        self.rewardWall = rewardWall
        self.rewardStep = rewardStep
        self._loadMemories()
        
        print("##### Initializing of DQNAgent finished")


    def _buildModel(self): #Angepasst
        print("##### Build Model")
        if os.path.isdir(MODEL_PATH):
            # Loading old network
            qModel = self.loadNetwork()

            # Loading old weights
            self.loadWeights(qModel)

        else:
            # Build the model with the functional keras API
            # Get the input tensor.
            inputs = Input(shape=(self.stateSize + self.actionSize,))

            # apppend the layers
            #x = Dense(512, activation='relu', kernel_regularizer=l2(0.0001))(inputs)
            
		    #x = Dense(512, activation='relu')(inputs)
            #x = Dense(256, activation='relu')(x)
            #x = Dense(128, activation='relu')(x)
            
            x = Dense(500, activation='relu', kernel_regularizer=l2(0.0001))(inputs)
            x = Dense(500, activation='relu', kernel_regularizer=l2(0.0001))(x)
            predictions = Dense(1, activation='linear')(x)

            # Create the model with the input & output layers
            qModel = Model(inputs=inputs, outputs=predictions)
            #qModel.compile(optimizer=Adam(lr=self.learningrate), loss='mse')
            qModel.compile(optimizer=Adam(), loss='mse')
        self.folderName = self._checkFolder()
        self.saveNetwork(qModel)

        print("##### Building finished")

        return qModel

    def _initActions(self):
        print("##### Creating ActionArray")
        actionLength = len(POSSIBLE_DIRECTIONS) * len(POSSIBLE_DIRECTIONS) - 1
        actions = np.zeros((actionLength, 2))
        actionIndex = 0
        for rl in range(len(POSSIBLE_DIRECTIONS)):
            for bf in range(len(POSSIBLE_DIRECTIONS)):
                if( not (POSSIBLE_DIRECTIONS[rl] == 0 and POSSIBLE_DIRECTIONS[bf] == 0)):
                    actions[actionIndex,:] = [POSSIBLE_DIRECTIONS[rl], POSSIBLE_DIRECTIONS[bf]]
                    actionIndex = actionIndex + 1

        print("##### Possible actions: \n {}".format(actions))
        print("##### ActionArray created")
        return actions

    def _softmax(self, Qa):
        return np.exp(Qa/self.tau) / np.sum(np.exp(Qa/self.tau))

    # Waehlt eine Aktion abhaengig von Qa aus und geibt den Index der Aktion zurueck
    def _chooseAction(self, Qa):
        # Array mit allen Indizes fuer Qa erzeugen
        toChoose = np.arange(0,len(Qa))
        Qa = np.squeeze(Qa)
        # Softmax anwenden
        pW = np.squeeze(self._softmax(Qa))

        # Wenn der Softmax NaN oder Inf zurueckgegeben hat,
        # wird eine zufaellige Aktion gewaehlt
        if np.any(np.isnan(pW)) or np.any(np.isinf(pW)):
            chosenA = np.random.randint(0,len(Qa))
        # Ansonsten wird mit den berechneten Gewichten eine Aktion gewaehlt
        else:
            testSum = np.sum(pW)
            if(testSum != 1):
                chosenA = np.random.randint(0,len(Qa))
            else:
                chosenA = np.random.choice(toChoose, replace=False, p=pW)
        # Den Index zurueckgeben
        return(chosenA)

    def _initialLearn(self):
        print("##### Initial learning")
        print("##### Needs a long time!")

        Xtrain = np.hstack((self.memoryStates[0:self.memoryCounter, :], self.memoryActions[0:self.memoryCounter, :]))
        callbackList = [EarlyStopping(monitor='loss',patience=5,verbose=0,mode='auto')]
        self.qModel.fit(Xtrain, self.alpha * self.memoryRewards[0:Xtrain.shape[0]], epochs=25, callbacks=callbackList, verbose=True)

    def _learn(self):
        print("##### _learn")
        badMemoryTreshold = self.rewardWall
        badMemoryTreshold = -0.4
        goodMemoryTreshold = self.rewardCollectable / 2
        goodMemoryTreshold = 2
        # ndarray erzeugen [0, 1, 2, 3, ... , memoryCounter -1]
        # Array wird benoetigt, da ein [,:] in der naechsten Zeile das ganze Array
        # und nicht nur die gespeicherten Datensaetze liefern wuerde
        learnSetIndices = np.arange(0, self.memoryCounter - 1)

        # Schlechte Erinnerungen suchen
        badMemoryIndices = np.flatnonzero(self.memoryRewards[learnSetIndices, 0] <= badMemoryTreshold)
        
        # ueberpruefen, ob wir mehr schlechte Erinnerungen haben, als beim Start festgelegt
        if badMemoryIndices.shape[0] > self.maxBadMemory * self.batchSize:
            # Zufaellige Menge aus den schlechten Erinnerungen waehlen
            randomIndices = np.random.choice(badMemoryIndices.shape[0], int(self.maxBadMemory * self.batchSize), replace = False)
            badMemoryIndices = badMemoryIndices[randomIndices]
        print("BadMemories: {}".format(len(badMemoryIndices)))
        
        # Gute Erinnerungen suchen
        goodMemoryIndices = np.flatnonzero(self.memoryRewards[learnSetIndices, 0] >= goodMemoryTreshold)
        
        # ueberpruefen, ob wir mehr gute Erinnerungen haben, als beim Start festgelegt
        if goodMemoryIndices.shape[0] > self.maxGoodMemory * self.batchSize:
            # Zufaellige Menge aus den guten Erinnerungen waehlen
            randomIndices = np.random.choice(goodMemoryIndices.shape[0], int(self.maxGoodMemory * self.batchSize), replace = False)
            goodMemoryIndices = goodMemoryIndices[randomIndices]
        print("GoodMemories: {}".format(len(goodMemoryIndices)))

        samplesPercentageLeft = 1 - self.maxGoodMemory - self.maxBadMemory

        # ueberpruefen ob wir noch genug Erinnerungen haben,
        # ansonsten die anzahl der restlichen Erinnerungen benutzen
        # samplesLeft = min(int(self.batchSize * samplesPercentageLeft), self.memoryCounter - badMemoryIndices.shape[0] - goodMemoryIndices.shape[0] - 1)
        samplesLeft = int(self.batchSize * samplesPercentageLeft)
        print("OtherMemories: {}".format(samplesLeft))
        # Die gerade bstimmte Anzahl an Erinnerungen aus den Daten auswaehlen
        if samplesLeft<self.memoryCounter - 1:
            additionalMemoryIndices = np.random.choice(self.memoryCounter - 1, samplesLeft, replace = False)
        else:
            additionalMemoryIndices = np.random.choice(self.memoryCounter - 1, self.memoryCounter - 1, replace = False)
        # Die schlechte und die zusaetzlichen Erinnerungen zusammenfassen -> mindestens 16% schlechte Erinnerungen
        learnSetIndices = np.hstack(( badMemoryIndices[None,:], goodMemoryIndices[None,:], additionalMemoryIndices[None,:]))
        learnSetIndices = np.reshape(learnSetIndices, learnSetIndices.shape[1])
        print("Indices: {}".format(len(learnSetIndices)))
        # Die Erinnerungen mit den States und Actions laden
        X = np.hstack((self.memoryStates[learnSetIndices,:], self.memoryActions[learnSetIndices,:]))

        # Achse mit der Laenge eins entfernen
        Qalt = np.squeeze(self.qModel.predict(X))
        actionLength = len(self.possibleActions)
        Qchoose = np.zeros((Qalt.shape[0], actionLength))
        actionIndex = 0
        # Durch alle moeglichen Actions iterieren
        # Zweite Moeglichkeit ist eine Q-Funktion fuer die Richtungen und eine
        # Q-Funktion fuer die Kraft mit der zuvor bestimmten Richtung
        for i in range(actionLength):
            # Naeherung fuer Qi der Q-Funktion auswerten
            action = self.possibleActions[actionIndex] *  np.ones_like(self.memoryActions[learnSetIndices, :])
            Xtemp = np.hstack((self.memoryStates[learnSetIndices + 1, :], action))
            Qchoose[:, actionIndex] = np.squeeze(self.qModel.predict(Xtemp))
            actionIndex = actionIndex + 1
        # Die Aktion auswaehlen, die die hoechte Belohnung bringt
        Qmax = np.max(Qchoose, axis=1)
        r = np.squeeze(self.memoryRewards[learnSetIndices])
        Qneu = ( 1 - self.alpha) * Qalt + self.alpha * ( r + self.gamma * Qmax)
        # Die Q-Funktion lernen
        callbackList = [EarlyStopping(monitor='loss',patience=2,verbose=0,mode='auto')]
        self.qModel.fit(X, Qneu, epochs=20, callbacks=callbackList, verbose=True)

        # Gewichte speichern
        self.saveWeights(self.qModel)

    def act(self, state):
        # In der Initialisierungsphase zufaellige Aktionen waehlen
        if self.initPhase:
            # Zufaellig eine Aktion auswaehlen
            chosenAction = np.squeeze(self.possibleActions[np.random.choice(self.possibleActions.shape[0], 1, replace = False), :])
        # Nach der Initialiserungsphase mit der Q-Funktion die Aktion waehlen
        else:
            # Leeres Array fuer die Rewards der moeglichen Aktionen anlegen
            Qa = np.zeros((self.possibleActions.shape[0], 1))
            # Durch alle Aktionen iterieren
            for i in range(self.possibleActions.shape[0]):
                # Aktion zwischenspeichern
                tempAction = self.possibleActions[i,:]
                # Inputarray fuer das Netz der Q-Funktion zusammenfuegen
                XTemp = np.hstack((state, tempAction))[np.newaxis]
                # Q-Funktion fuer die einzelnen Aktionen auswerten
                Qa[i] = np.squeeze(self.qModel.predict(XTemp))
            # Aktion aus allen Moeglichkeiten auswaehlen
            chosenActionIndex = self._chooseAction(Qa)
            print(Qa)
            print()
            chosenAction = self.possibleActions[chosenActionIndex, :]
            #print(chosenAction)
        
        # Ausgewaehlte Aktion zurueckgeben
        return chosenAction

    def remember(self, state, action, reward, saveMemories = False):
        print('Reward: {}'.format(reward))
        # MemoryCounter inkrementieren
        self.memoryCounter += 1
        # Zustand, gewaehlte Aktion und die Belohnung speichern
        self.memoryStates[self.memoryCounter, :] = state
        self.memoryActions[self.memoryCounter, :] = action
        self.memoryRewards[self.memoryCounter] = reward

        # Solange wir weniger als 500 Erinnerungen haben, lernen wir nicht
        if self.initPhase and self.memoryCounter > self.initialLearnTreshold:
            # Einmal mit allen Daten lernen
            self._initialLearn()
            self.initPhase = False
            print("##### Initialisierungsphase abgeschlossen")
            print("##### MemoryCounter: " + str(self.memoryCounter))
        # Wenn die Initialisierungsphase vorbei ist, lernen wir all X Eintraege.
        elif not self.initPhase and self.memoryCounter % self.updateFrequency == 0:
            # Lernen
            self._learn()
            print("##### MemoryCounter: " + str(self.memoryCounter))
        # Erinenerungen in die Datei schreiben, wenn es explizit gefordert wird, oder das Intervall erreicht wird
        if saveMemories or (self.memoryCounter > 0 and self.memoryCounter % self.saveMemoriesFrequency == 0):
            self.saveMemories()

    # Checks the Folder for Saving the Network
    # Creates Folder like this: YYYYMMDD-N
    # Increaeses N after every run of the Brain
    # Creates one file with the network configuration
    def _checkFolder(self):
        print("##### Checking Folder ...")
        now = datetime.datetime.now()
        todaysFoldersPrefix = "{:04d}{:02d}{:02d}-".format(now.year, now.month, now.day)

        highestIndex = -1

        # Hoechsten verwendeten Index finden
        for dirpath, dirnames, filenames in os.walk(MODEL_PATH):
            for directory in dirnames:
                if(str(directory).startswith(todaysFoldersPrefix)):
                    index = int(str(directory).split("-")[1])
                    if(index > highestIndex):
                        highestIndex = index
            # break, damit wir nur das erste Verzeichnis durchsuchen
            break

        # Index inkrementieren
        highestIndex += 1

        # Create Folder
        folderName = todaysFoldersPrefix + str(highestIndex)
        newPath = MODEL_PATH + folderName
        os.makedirs(newPath)

        print("##### Using Folder {} for network saving".format(folderName))

        return folderName



    # Saves the network inside current Folder
    def saveNetwork(self, model):
        jsonModel = model.to_json()
    
        path = "{0}{1}/model.json".format(MODEL_PATH, self.folderName)

        with open(path, "w") as jsonFile:
            #json.dump(jsonModel, jsonFile)
            jsonFile.write(jsonModel)

        path = "{0}{1}/kerasModel.h5".format(MODEL_PATH, self.folderName)
        model.save(path)

    # Loads the least saved Network into the Agent
    def loadNetwork(self):
        path = "{0}*/".format(MODEL_PATH)
        newestpath = max(glob.iglob(path), key=os.path.getctime)
        path = "{0}model.json".format(newestpath)
        print("### Loading old network from Path: {0}".format(path))
        with open(path, "r") as jsonFile:
            readjsonFile = jsonFile.read()
        loadedModel = model_from_json(readjsonFile)
        loadedModel.compile(optimizer=Adam(), loss='mse')
        return loadedModel


    def saveWeights(self, model):
        self.saveCounter += 1

        path = "{0}{1}/{2}-{3}.h5".format(MODEL_PATH, self.folderName, self.saveCounter, self.memoryCounter)

        model.save_weights(path)

    def loadWeights(self,model):
        path = "{0}*/*.h5".format(MODEL_PATH)
        newestpath = max(glob.iglob(path), key=os.path.getmtime)
        print("### Loading old weights from Path: {0}".format(newestpath))
        model.load_weights(newestpath)
        

    def saveMemories(self):
        print("##### Saving Memories ...")

        memories = np.hstack((self.memoryStates, self.memoryActions, self.memoryRewards))
        rewards = np.array((self.rewardGoal, self.rewardCollectable, self.rewardWall, self.rewardStep, self.memoryCounter))

        np.save(MEMORIES_PATH, memories)
        np.save(REWARDS_PATH, rewards)

        print("##### Memories saved")

    def _loadMemories(self):
        print("##### Loading old memories ...")
        if os.path.isfile(MEMORIES_PATH):
            # Erinnerungen laden
            memories = np.load(MEMORIES_PATH)
            # Die Rewardvorgaben fuer diese Erinnerungen laden. Die 5. Position in diesem Array ist der MemoryCounter
            rewardDefinition = np.load(REWARDS_PATH)

            # States und Actions "slicen"
            self.memoryStates = memories[:,:self.stateSize]
            self.memoryActions = memories[:,self.stateSize:self.stateSize+self.actionSize]
            # Rewards zwischenspeichern
            rewards = memories[:,self.stateSize + self.actionSize]

            # Rewards auf neue Vorgaben aktualisieren und dann speichern
            rewards[rewards == rewardDefinition[0]] = self.rewardGoal
            rewards[rewards == rewardDefinition[1]] = self.rewardCollectable
            rewards[rewards == rewardDefinition[2]] = self.rewardWall
            rewards[rewards == rewardDefinition[3]] = self.rewardStep

            self.memoryRewards = np.reshape(rewards, (len(rewards), 1))

            # MemoryCounter auslesen
            self.memoryCounter = int(rewardDefinition[4])
            print(self.memoryCounter)
            self.initPhase = False
            print("##### Old Memories loaded")

            # Mit den alten Erinnerungen lernen
            if(self.memoryCounter >= self.initialLearnTreshold):
                for i in range(10):
                    self._learn()
        else:
            print("##### No old memories found.")








