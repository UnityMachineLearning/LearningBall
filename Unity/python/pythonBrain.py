##### Imports #####
from unityagents import UnityEnvironment
import matplotlib.pyplot as plt
import numpy as np
import sys
import time
import pygame
import math
import csv
from dqnAgent import DQNAgent
##### End Imports #####


##### Params #####
# Name of the environment binary (located in the root directory of the python project).
UNITY_FILENAME = "../Builds/LearningBall/LearningBall"
# Indicates which port to use for communication with the environment. For use in parallel training
UNITY_WORKERID = 0
# Seed
UNITY_SEED = 1
# Agentnumber
AGENT_NUMBER = 0

# TrainMode
TRAIN_MODE = True
# Number of Episodes
MAX_EPISODES = 10000
# Network-Size
STATESIZE = 20
ACTIONSIZE = 2

# Frequency for acting
ACT_FREQUENCY = 25

# Rewards
REWARD_WALL = -100
REWARD_GOAL = 1000
REWARD_COLLECTABLE = 500
REWARD_STEP = -0.01
##### End Params #####



##### Code #####

print("Python version:")
print(sys.version)

# check Python version
if (sys.version_info[0] < 3):
    raise Exception("ERROR: ML-Agents (v0.3 onwards) requires Python 3")

# Creating the environment
print("##### Start UnityEnvironment. This may take a while...")
unityEnvironment = UnityEnvironment(UNITY_FILENAME, UNITY_WORKERID, UNITY_SEED)
print("##### UnityEnvironment started.")
# print(str(unityEnvironment))

# Create the agent
agent = DQNAgent(stateSize = STATESIZE, actionSize = ACTIONSIZE, rewardGoal = REWARD_GOAL, rewardCollectable = REWARD_COLLECTABLE, rewardWall = REWARD_WALL, rewardStep = REWARD_STEP)

# Get the default brain --> I think, that we control multiple brains with indexing
defaultBrainName = unityEnvironment.brain_names[0]
brain = unityEnvironment.brains[defaultBrainName]

# Initialize pygame for Userinput
userInput = False

if(userInput):
    pygame.init()
    pygame.display.set_mode((100, 100))

print("##### Start the game ...")
for episode in range(MAX_EPISODES):
    # Reset the environment
    brainInfo = unityEnvironment.reset(train_mode=TRAIN_MODE)[defaultBrainName]
    episodeDone = False
    episodeRewards = 0
    episodesSteps = 0
    wallsHit = 0
    collectedCollectables = 0
    action = [0, 0]
    realReward = 0

    # Run the episode
    while not episodeDone:
        # Increase the stepcounter
        episodesSteps += 1

        # Get the current state
        state = np.squeeze(np.transpose(brainInfo.vector_observations))
        #print('State: {}'.format(state))
		
        # Create the actionDict for the UnityLib
        actionDict = {defaultBrainName:action}
        #print('ActionDict: {}'.format(actionDict))
		
		# Perform the action in Unity
        brainInfo = unityEnvironment.step(vector_action=actionDict)[defaultBrainName]

        # Get the information from the brain
        stepReward = brainInfo.rewards[AGENT_NUMBER]
		
        episodeDone = brainInfo.local_done[AGENT_NUMBER]

        # Ziel erreicht
        if(stepReward >= 100):
            realReward += REWARD_GOAL
            stepReward -= 100
        # Collecatbale gesammelt
        elif(stepReward >= 10):
            realReward += REWARD_COLLECTABLE
            stepReward -= 10
            collectedCollectables += 1
        # Wand getroffen
        elif(stepReward >= 1):
            realReward += REWARD_WALL
            stepReward -= 1
            wallsHit += 1        
        # Pro Step negativen Reward verteilen
        else:
            # _rewardDistance = 3 * 0.0728
            _distanceRewards = 0
            eastWestWallPositionX = 0.11
            northSouthWallPositionZ = 0.15
            ballRadius = 0.00364
            _distanceToWallsEastWest = eastWestWallPositionX - abs(state[0]) - ballRadius
            _distanceToWallsNorthSouth = northSouthWallPositionZ - abs(state[1]) - ballRadius
            #print('Distances NS: {}, EW: {}'.format(_distanceToWallsNorthSouth, _distanceToWallsEastWest))
            # Distanzen berechen und fuer die kleinste den Reward verteilen
            for i in range(4, 16, 3):
                _distanceToCollectable = math.sqrt(state[i]**2 + state[i+1]**2)
                if(int(state[i+2]) == 1):
                    _distanceRewards += 10 * np.exp(-50 *_distanceToCollectable)
            _distanceRewards += REWARD_STEP
            _distanceRewards += - 0.5 * np.exp(-50 * _distanceToWallsEastWest)
            _distanceRewards += - 0.5 * np.exp(-50 * _distanceToWallsNorthSouth)
            realReward += _distanceRewards

        if episodesSteps % ACT_FREQUENCY == 0:
            if(userInput):
                action = [0, 0]
                pygame.event.pump()
                keys = pygame.key.get_pressed()
                if(keys[pygame.K_w]):
                    action[0] += 0
                    action[1] += 1
                if(keys[pygame.K_a]):
                    action[0] += -1
                    action[1] += 0
                if(keys[pygame.K_s]):
                    action[0] += 0
                    action[1] += -1
                if(keys[pygame.K_d]):
                    action[0] += 1
                    action[1] += 0
                if(keys[pygame.K_u]):
                    print("##### End of Userinput")
                    userInput = False
                    pygame.quit()
            else:
                # Get the action from the agent
                action = agent.act(state)

            episodeRewards += realReward

            # Learn
            agent.remember(state, action, realReward)
			
            realReward = 0

            if(episodesSteps / ACT_FREQUENCY >= 10000):
                episodeDone = True

        if episodeDone:
            print("##### Game {}/{} finished, Reward: {}, Steps: {}, Walls hit: {}, Collected: {}".format(episode, MAX_EPISODES, episodeRewards, episodesSteps, wallsHit, collectedCollectables))
            writer = csv.writer(open("GamesDone.csv", "a"))
            writer.writerow([episode, episodeRewards, episodesSteps, wallsHit, collectedCollectables])
    


# Close the environment and close the connection
unityEnvironment.close()